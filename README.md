# Music Utilities
The software here is a collection of found python recipes that I put together 
to help me clean up old ripped MP3 music files.  Some files were missing artist names.
Some were missing song titles.  The recipes in this project helped me understand the
MP3 format and how to manipulate it.
