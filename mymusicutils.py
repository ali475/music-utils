#!/usr/bin/env python
#
#
# Copyright (c) 2017 Shaheen H. Ali
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

"""
A collection of utilities
"""
import os
import pydub


tagnames=['track', 'artist', 'bit_rate', 'album', 'genre', 'language' ]

def getmeta(fname):
    """
    Pull out the relevant tags and put them into a dictionary
    """
    rawdict = pydub.utils.mediainfo(fname)
    cooked = {}
    for tag in tagnames:
        cooked[tag] = rawdict[tag]
    return cooked

def getformat(fname):
    return pydub.utils.mediainfo(fname)['format_name']

def getallmeta(fname):
    """
    Pull out all the tags and return the dictionary
    """
    return pydub.utils.mediainfo(fname)

def getType(fname):
    """
    Determine the music file type
    """
    comps = fname.split('.')
    return comps[-1]

def changeSuffix(fname, suffix):
    """
    Replace the suffix of a specific filename
    """
    return os.path.splitext(fname)[0] + suffix    

if __name__ == '__main__':
    import sys

    if len(sys.argv) < 2:
        print 'Argument required'
        sys.exit(1)

    print getType(sys.argv[1])
    sys.exit(0)
